#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
char input[128];
char cmd[64], arg[128];
char pathcmd[128];
char *args[100];
char *path;
char *paths[128];
char *filename,*filemode;
char *homedir;
int n;

int splitcmd(char *arglist){
	n = 0;
	char *s;
	s = strtok(arglist, " ");
	strcpy(cmd,s);
	args[n] = s;
	while(s){
		n++;
		s = strtok(0," ");
		args[n] = s;
	}
	n--;
	return n;	

}

int splitfile(char *str){
	char *io = ">>";
	if (strstr(str,io)){
		printf("\n>>\n");
		filemode = ">>";
		strcpy(input,strtok(str,filemode));
		filename = strtok(0,filemode);	
		return 0;
	}
	io = ">";
	if (strstr(str,io)){
		printf("\n>\n");
		filemode = ">";
		strcpy(input,strtok(str,filemode));
		filename = strtok(0,filemode);	
		return 0;
	}		
	io = "<";
	if (strstr(str,io)){
		printf("\n<\n");
		filemode = "<";
		strcpy(input,strtok(str,filemode));
		filename = strtok(0,filemode);	
		return 0;
	}
	
}

int tokenize(char *str,char *delim, char *output[]){
	int i = 0;
	char *s;
	s = strtok(str,delim);
	while(s){
		s = strtok(0,delim);
		output[i] = s;
		i++;
	}
	return i;	

}

int redirectinput(){
	if (filename[0] == ' '){
		filename += 1;
	}
	printf("in redirect\n");
	if (!strcmp(filemode, ">>")){
		printf("\nFilemode = %s\n",filemode);
		printf("Filename = %s\n",filename);
		close(1);
		open(filename, O_WRONLY|O_APPEND|O_CREAT, 0644);	
		return 0;
	}
	if (!strcmp(filemode, ">")){
		printf("\nFilemode = %s\n",filemode);
		printf("Filename = %s\n",filename);
		close(1);
		open(filename, O_WRONLY|O_CREAT, 0644);	
		return 0;
	}		
	if (!strcmp(filemode, "<")){
		printf("\nFilemode = %s\n",filemode);
		printf("Filename = %s\n",filename);
		close(0);
		open(filename, O_RDONLY);	
		return 0;
	}
}

int cd(char *arg){
	printf("PROC %d \n", getpid());
	char p[128];
	if (arg == NULL){
		strcpy(p,homedir);
	}
	else {strcpy(p,arg);}
	if(chdir(p) == 0){
		printf("Changed current directory to %s\n",p);
	}
	else{ printf("Failed to change directory to %s\n",p);
	}
	return 0;
}

int mymkdir(char *arg){
	printf("PROC %d \n", getpid());
	char p[128];
	if (arg == NULL){
		printf("Must specify directory name\n");
	}
	else {strcpy(p,arg);}
	if(mkdir(p, 0766) == 0){
		printf("Made directory %s\n",p);
	}
	else{ printf("Failed to make directory %s\n",p);
	}
	return 0;
}

int myrmdir(char *arg){
	printf("PROC %d \n", getpid());
	char p[128];
	if (arg == NULL){
		printf("Must specify directory name\n");
	}
	else {strcpy(p,arg);}
	if(rmdir(p) == 0){
		printf("Removed directory %s\n",p);
	}
	else{ printf("Failed to remove directory %s\n",p);
	}
	return 0;
}

int quit(char *na){
	printf("PROC %d \n", getpid());
	exit(1);
	return 0;
}

int bash(char *env[]){
	int pid, status;
	char buf[256];
	FILE *infile;
	pid = fork();
	if (pid < 0){
		perror("Fork Failed");
		quit(args[1]);
	}
	if (pid){
		//PARENT WAITS ON CHILD TO DIE
		pid = wait(&status);	
	}else{
		//CHILD PERFORMS COMMAND
		printf("PROC %d \n", getpid());
		printf("In bash\n");
		strcat(pathcmd, "/bin/");
		strcat(pathcmd, cmd);
		infile = fopen(args[1], "r");
		if(infile == NULL){ printf("Could not open file\n"); quit("");return 0;}
			
		for (int i = 0; i< 256; i++){
			buf[i] = fgetc(infile);
		}
		fclose(infile);
		if(!strncmp(buf, "#!", 2)){
			printf("Executing bash file \n");
			int r = execve(pathcmd,args,NULL);	
		}
	}
}

int execute(char *env[]){
	printf("PROC %d \n", getpid());
	int r = -1;
	int p = 0;
	while(r == -1){
		if(paths[p]){
			strcpy(pathcmd, paths[p]);
			strcat(pathcmd, "/");
			strcat(pathcmd, cmd);
			printf("cmd %d = %s\n",p, pathcmd);	
			/*
			printf("args[0] = %s\n", args[0]);
			printf("args[1] = %s\n", args[1]);
			printf("env = %s\n",env[0]);
			*/
			printf(" Args: %s %s %s \n", args[0] , args[1] , args[2]);
			r = execve(pathcmd,args,env);
			printf("Command was not found in location\n");
		}
		p++;	
	}
	return 0;	
}

int	startproc(char *env[]){
	int pid, status;
	pid = fork();
	if (pid < 0){
		perror("Fork Failed");
		quit(args[1]);
	}
	if (pid){
		//PARENT WAITS ON CHILD TO DIE
		pid = wait(&status);	
	}else{
		//CHILD PERFORMS COMMAND
		if (filemode != 0){
			printf("Going into redirect\n");
			redirectinput();
		}
		execute(env);				
	}
}

char *cmdd[] = {"cd","exit","mkdir","rmdir"};

int findCmd(char *command){
	int i = 0;
	while(cmd[i]){
		if (!strcmp(command,cmdd[i])){
			return i; }
		i++;
	}
	return -1;
}

int (*fptr[])(char *) = { (int (*) ())cd,quit,mymkdir,myrmdir};

int main(int argc, char *argv[], char *env[]){
	homedir = getenv("HOME");	
	path = getenv("PATH");
	printf("Homedir = %s\n",homedir);
	printf("Path = %s\n",path);
	tokenize(path,":",paths);
	int i = 0;
	while(paths[i]){
		printf("Path %d = %s\n",i,paths[i]);
		i++;
	}

	printf("Inside pseudo shell: \n");
	int index;
	char buff[256],*cwd;
	while(1){
		filemode = 0;
		//Get the cwd and print out
			//just for looks
		
		cwd = getcwd(buff,256);
		printf("~%s/ ",cwd);
		//Get user input into input
		fgets(input,128,stdin);
		while(input[0] == '\0' || input[0] == '\n'){
			cwd = getcwd(buff,256);
			printf("~%s/ ",cwd);
			//Get user input into input
			fgets(input,128,stdin);
		}
		//output what was read
		printf("input = %s\n",input);
		if (input[0] == '\0')
			printf("invalid input");
		input[strlen(input)-1] = 0;
		//splits input into
			//
		splitfile(input);
		splitcmd(input);
		if(!strcmp(cmd,"bash")){
			int r = bash(env);
		}else{
			index = findCmd(cmd);
			if (index == -1){
				int r = startproc(env);
			}else{
				int r = fptr[index](args[1]);
			}
		}
	}
	return 0;	
}

