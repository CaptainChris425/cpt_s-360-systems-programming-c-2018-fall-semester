#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ext2fs/ext2_fs.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>

//#include "type.h"
//#include "util.o"
#include "util.c"
#include "cd_ls_pwd.c"

MINODE minode[NMINODE];
MINODE *root;
PROC   proc[NPROC], *running;

char gpath[128]; // holder of component strings in pathname
char *name[64];  // assume at most 64 components in pathnames
int  n;

//int  fd, dev;
//int  nblocks, ninodes, bmap, imap, iblk, inode_start;
//char line[256], cmd[32], pathname[256];

/******* WRITE YOUR OWN util.c and others ***********
***************************************************/

int init()
{
  int i,j;
  MINODE *mip;
  PROC   *p;

  printf("init()\n");
	//initialize all minodes as free
  for (i=0;i<NMINODE;i++)
	minode[i].refCount = 0;
 
	//Initialize all mtable entries as free
/*
  for (i=0;i<NMOUNT;i++)
	mtable[i].refCount = 0;
*/
   
  for (i=0;i<NPROC;i++){
	proc[i].status = 0; //Ready
	proc[i].pid = i; //pid = i
	proc[i].uid = i; //uid = i 
	for (j=0;j<NFD;j++){
		proc[i].fd[j] = 0; //file descriptors = 0
	}
	proc[i].next = &proc[i+i]; //linked list
  }
  proc[NPROC-1].next = &proc[0]; //circularly linked list
  running = &proc[0];
/*
  for (i=0; i<NMINODE; i++){
      mip = &minode[i];
	  mip->dev = 0;
	  mip->ino = 0;
	  mip->refCount = 0;
	  mip->dirty = 0;
	  mip->mounted = 0;
      // set all entries to 0;
  }
  for (i=0; i<NPROC; i++){
       p = &proc[i];
	   p->pid = i;
	   p->uid = i;
	   p->cwd = 0;
      // set pid = i; uid = i; cwd = 0;
  }
  for (i=0; i<NPROC; i++){
       p = &proc[i];
	   printf("pid[%d] pid = %d, ",i,p->pid);
	   printf("uid = %d, ",p->uid);
	   printf("cwd = %d\n",p->cwd);
      // set pid = i; uid = i; cwd = 0;
  }
*/
}

// load root INODE and set root pointer to it
int mount_root()
{  
  char buf[BLKSIZE];
  SUPER *sp;
  GD    *gp;
/*
	
  (1). read super block in block #1;
       verify it is an EXT2 FS;
       record nblocks, ninodes as globals;
*/
  printf("Mounting root filesystem...  ");
  //calling function to check if the fd is a EXT2 file system
	//also puts super block in sp
  sp = super(fd,buf, sp);
  if (!sp){
	printf("Trying to mount a non-Ext2 filesystem\n");	
	exit(0);
  }
  nblocks = sp->s_blocks_count; //Global variables in type.h
  ninodes =	sp->s_inodes_count; //Global variables in type.h
  printf("OK\n");
  printf("File system:\n\tnblocks = %d\n\tninodes = %d\n",nblocks,ninodes);
/*
  (2). get GD0 in Block #2:
       record bmap, imap, inodes_start as globals
*/
  //calling function to get group descriptor in gp
  printf("Recording group descriptor values... ");
  gp = groupdesc(fd,buf,gp);
  bmap = gp->bg_block_bitmap;//Global variables in type.h
  imap = gp->bg_inode_bitmap;//Global variables in type.h
  inode_start = gp->bg_inode_table;//Global variables in type.h
  
  printf("OK\n");
  printf("Group Descriptor values:\n\tbmap = %d\n\timap = %d\n\tinode_start = %d\n", \
		bmap,imap,inode_start);
/*
  (3). root = iget(dev, 2);       // get #2 INODE into minoe[ ]
       printf("...Mounted root OK\n");
*/
 root = iget(fd,2);  
  

}

int quit()
{
  int i;
  MINODE *mip;
  for (i=0; i<NMINODE; i++){
    mip = &minode[i];
    if (mip->refCount > 0)
      iput(mip);
  }
  exit(0);
}


char *disk = "mydisk";
int main(int argc, char *argv[ ])
{
  int ino;
  char buf[BLKSIZE];
  if (argc > 1)
     disk = argv[1];

  fd = open(disk, O_RDONLY);//O_RDWR);
  if (fd < 0){
     printf("Opening disk %s failed... exiting\n", disk);  
     exit(1);
  }
  dev = fd;
  init();  
  mount_root();
  printf("Root refCount = %d\n", root->refCount);

  printf("Creating P0 as running process... ");
  running = &proc[0];
  running->status = 1;//READY;
  running->cwd = iget(dev, 2);
  printf("OK\n");
  printf("Setting P1 cwd to root... ");
  proc[1].cwd = iget(dev, 2);

  printf("OK\n");
  // set proc[1]'s cwd to root also
  printf("Root refCount = %d\n", root->refCount);

  while(1){
    printf("input command : [ls|cd|pwd|quit] ");
    fgets(line, 128, stdin);
    line[strlen(line)-1] = 0;

    if (line[0]==0)
       continue;
    pathname[0] = 0;

    sscanf(line, "%s %s", cmd, pathname);
    printf("cmd=%s pathname=%s\n", cmd, pathname);

    if (strcmp(cmd, "ls")==0)
       ls(pathname);

    if (strcmp(cmd, "cd")==0)
       cdir(pathname);

    if (strcmp(cmd, "pwd")==0)
       pwd(running->cwd);

    if (strcmp(cmd, "quit")==0)
       quit();
  }
}

int print(MINODE *mip)
{
  int blk;
  char buf[1024], temp[256];
  int i;
  DIR *dp;
  char *cp;

  INODE *ip = &mip->INODE;
  for (i=0; i < 12; i++){
    if (ip->i_block[i]==0)
      return 0;
    get_block(dev, ip->i_block[i], buf);

    dp = (DIR *)buf; 
    cp = buf;

    while(cp < buf+1024){

       // make dp->name a string in temp[ ]
       printf("%d %d %d %s\n", dp->inode, dp->rec_len, dp->name_len, temp);

       cp += dp->rec_len;
       dp = (DIR *)cp;
    }
  }
}
 

