#! /bin/bash
dd if=/dev/zero of=mydisk bs=1024 count=1440
mkfs -b 1024 mydisk 1440
mount -o loop mydisk /mnt
(cd /mnt; rmdir lost+found; mkdir dir1 dir1/dir11 dir1/dir11/dir12 dir2 dir3 dir4; touch dir1/file1 file2 file3 file4 dir1/dir11/dir12/foundme; ls -l -a)
umount /mnt
