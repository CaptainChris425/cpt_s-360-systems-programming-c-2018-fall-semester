#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <libgen.h>

struct stat mystat, *sp;
char *t1 = "xwrxwrxwr-------";
char *t2 = "----------------";

int ls_file(char *fname)
{
		printf("In ls_file with %s", fname);
		FILE *file;
        struct stat fstat, *sp;
        int r, i;
        char ftime[64];
		char checkfilename[256];	
		strcpy(checkfilename,fname);
		if (strcmp(basename(checkfilename),"serverlsfile.txt") == 0) return 0;
		strcpy(checkfilename,fname);
		if (strcmp(basename(checkfilename),"clientlsfile.txt") == 0) return 0;
		//file = fopen("randfile.txt","a");
		//fprintf(file, "i hate this\n");
		//fclose(file);
		//file = fopen("randfile.txt","a");
		//fprintf(file, "i also hate this\n");
		//fclose(file);
		file = fopen("serverlsfile.txt","a");
        sp = &fstat;
        printf("name=%s\n", fname); //getchar();

        if ( (r = lstat(fname, &fstat)) < 0){
                fprintf(file,"Could not stat %s | %d", fname,r);
                return -1;
        }

        if ((sp->st_mode & 0xF000) == 0x8000)
                fprintf(file,"%c",'-');
        if ((sp->st_mode & 0xF000) == 0x4000)
                fprintf(file,"%c",'d');
        if ((sp->st_mode & 0xF000) == 0xA000)
                fprintf(file,"%c",'l');

        for (i=8; i >= 0; i--){
                if (sp->st_mode & (1 << i))
                    fprintf(file,"%c", t1[i]);
                else
                    fprintf(file,"%c", t2[i]);
        }
        fprintf(file,"%4d ",sp->st_nlink);
        fprintf(file,"%4d ",sp->st_gid);
        fprintf(file,"%4d ",sp->st_uid);
        fprintf(file,"%8d ",sp->st_size);

        // print time
        strcpy(ftime, ctime(&sp->st_ctime));
        ftime[strlen(ftime)-1] = 0;
        fprintf(file,"%s  ",ftime);
        // print name
        fprintf(file,"%s", basename(fname));

        // print -> linkname if it's a symbolic file
        if ((sp->st_mode & 0xF000)== 0xA000){ // YOU FINISH THIS PART
                // use readlink() SYSCALL to read the linkname
                // printf(" -> %s", linkname);
        }
		fprintf(file,"\n");
		fclose(file);
}

int ls_dir(char *dname)
{
        char tempd[1024];
        DIR *dirToOpen = opendir(dname);
        struct dirent *FileOrDir;
        while(FileOrDir = readdir(dirToOpen)){
                strcpy(tempd,dname);
                strcat(tempd,"/");
                strcat(tempd,FileOrDir->d_name);
                ls_file(tempd);
                bzero(tempd,1024);
        }
        close(dirToOpen);
}

int myls(char *path)
{
		FILE * clearfile;
		clearfile = fopen("serverlsfile.txt","w");
		fclose(clearfile);	
		printf("AAAtleast im in here \n");
        struct stat mystat, *sp;
        int r;
        char *s;
        char name[1024], cwd[1024];
		printf("did i make it\n");	
        s = path;
        if (path[0] == '\0'){
                strcpy(s, "./");
		}
		
        sp = &mystat;
        if (r = lstat(s, sp) < 0){
                printf("no such file %s\n", s); return(1);
        }
        strcpy(name, s);
        if (s[0] != '/'){    // name is relative : get CWD path
                getcwd(cwd, 1024);
                strcpy(name, cwd); strcat(name, "/"); strcat(name,s);
        }
        if (S_ISDIR(sp->st_mode)){
                ls_dir(name);
		}
        else{
                ls_file(name);
		}

}

