#include <stdio.h>
#include <stdlib.h>
int getfibnum(int);
int main(){
	for (int i = 1; i<= 10; i++){
		printf("The %d number in the fib sequence is %d\n",i,getfibnum(i));
	}
}

int getfibnum(int n){
	if (n<=1){ return 1;};
	int fib = 0, n1 =0, n2 =1;
	for(int i =0; i<n-1; i++){
		fib = (n1+n2);
		n1 = n2;
		n2 = fib;
	}
	return fib;
}



