/******** C13.2.b: TCP client.c file TCP ********/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#define MAX 256
#define SERVER_HOST "localhost"
#define SERVER_PORT 1234
struct sockaddr_in server_addr;
int sock, r;
char cmd[256],pathname[256];

int get(char *pathname);
int put(char *pathname);

int client_init()
{
		printf("======= clinet init ==========\n");
		printf("1 : create a TCP socket\n");
		sock = socket(AF_INET, SOCK_STREAM, 0);
		if (sock<0){
			printf("socket call failed\n"); exit(1);
		}
		printf("2 : fill server_addr with server’s IP and PORT#\n");
		server_addr.sin_family = AF_INET;
		server_addr.sin_addr.s_addr = htonl(INADDR_ANY); // localhost
		server_addr.sin_port = htons(SERVER_PORT); // server port number
		printf("3 : connecting to server ....\n");
		r = connect(sock,(struct sockaddr*)&server_addr, sizeof(server_addr));
		if (r < 0){
			printf("connect failed\n"); exit(3);
		}
		printf("4 : connected OK to\n");
		printf("-------------------------------------------------------\n");
		printf("Server hostname=%s PORT=%d\n", SERVER_HOST, SERVER_PORT);
		printf("-------------------------------------------------------\n");
		printf("========= init done ==========\n");
}
int main()
{
		int n;
		char line[MAX], ans[MAX];
		client_init();
		printf("******** processing loop *********\n");
		while (1){
				printf("input a line : ");
				bzero(line, MAX);
				// zero out line[ ]
				fgets(line, MAX, stdin);
				// get a line from stdin
				line[strlen(line)-1] = 0;
				// kill \n at end
				if (line[0]==0)
					// exit if NULL line
					exit(0);
				// Send line to server
				//bzero(cmd, 256);
				//bzero(pathname, 256);
				cmd[0] = 0;
				pathname[0] = 0;
				sscanf(line,"%s %s",cmd,pathname);
				printf("cmd %s pathname %s\n",cmd, pathname);
				n = write(sock, line, MAX);
				if (strcmp(cmd,"get")==0){
					if(get(pathname))
						printf("Successfully got %s\n",pathname);
				}
				else if (strcmp(cmd,"put")==0){
					put(pathname);
				}
				else if (strcmp(cmd,"ls")==0){
				//	n = read(sock, ans, MAX);
					int temp = 0;
					if(get("clientlsfile.txt")){
						FILE *readls;
						readls = fopen("clientlsfile.txt","r");
						char c;
						c = fgetc(readls);
						while(c != EOF){
							printf("%c",c);
							c = fgetc(readls);	
						}	
						fclose(readls);
						unlink("clientlsfile.txt");
						printf("\n");
					}
				}
				else if (strcmp(cmd, "pwd") == 0){
					n = read(sock, ans, MAX);
					printf("%s\n",ans);	

				}
				else if (strcmp(cmd, "rmdir") == 0){
					if(strcmp(pathname,"") == 0)
						printf("Need to specify directory\n");
					else
						printf("Removing dir %s\n",pathname);
				}
				else if (strcmp(cmd, "rm") == 0){
					if(strcmp(pathname,"") == 0)
						printf("Need to specify file\n");
					else
						printf("Removing file %s\n",pathname);
				}
				else if (strcmp(cmd, "cd") == 0){
					printf("Changing directory to %s\n", (strcmp(pathname, "") == 0) ? "/": pathname);
				}
				else if (strcmp(cmd, "pwd") == 0){
					n = read(sock, ans, MAX);
					printf("%s\n",ans);	

				}
				else{
					printf("client: wrote n=%d bytes; line=%s\n", n, line);
				//	Read a line from sock and show it
					n = read(sock, ans, MAX);
					printf("client: read n=%d bytes; echo=%s\n", n, ans);
				}
		}
}



int get(char *pathname)
{
	//printf("In get with pathname %s\n",pathname);
	char line[MAX], lps[MAX],ans[MAX];
	int n;
	FILE *file;
	int loops;
	int i;
	file = fopen(pathname,"w");	
	n = read(sock ,lps,MAX);
	loops = atoi(lps);
	//printf("Loops = %d\n",loops);
	for	(i = 0; i < loops; i++){
		n = read(sock, ans,MAX);
		//printf("[%d] %c\n",i, ans[0]);
	//	fprintf(file, "lets just see");
		fputc(ans[0],file);
		//fputs(ans, file);
	}
	fclose(file);
	return 1;

}

int put(char *pathname)
{
	 printf("Getting %s\n",pathname);
     FILE *file;
     int i;
	 int n;
     char onechar[MAX];
     char size[126];
     int sz;
     //file = fopen(pathname,"r");
     file = fopen(pathname,"r");
     if (file < 0){
         printf("Could not open file\n");
         n = write(sock,"-1",MAX);
         return 0;
     }
     fseek(file, 0L, SEEK_END);
     sz = ftell(file);
     fseek(file, 0L, SEEK_SET);
     //sz = (sz/256)+1;
     //itoa(sz,size,10); 
     sprintf(size, "%d", sz);
     n = write(sock,size,MAX);
     for (i = 0; i < sz; i++){
         //fgets(line,MAX,file);
         onechar[0] = fgetc(file);
         //printf("[%d] %c\n", i, onechar);
         //printf("before sending\n");
         n = write(sock,onechar,MAX);
         //printf("Sent it \n");
     }
     fclose(file);
}

