#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ext2fs/ext2_fs.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#define BLKSIZE 1024

// define shorter TYPES, save typing efforts
typedef struct ext2_group_desc  GD;
typedef struct ext2_super_block SUPER;
typedef struct ext2_inode       INODE;
typedef struct ext2_dir_entry_2 DIR;    // need this for new version of e2fs

GD    *gp;
SUPER *sp;
INODE *ip;
DIR   *dp;

int fd;
int iblock;

char *path[256];
int n, i;

int tokenize(char *pathname){
    n = 0;
    char *s;
    s = strtok(pathname,"/");
    while(s){
        path[n] = s;
        n++;
        s = strtok(0,"/");
    }
    return n;

}

int get_block(int fd, int blk, char buf[ ])
{
  lseek(fd,(long)blk*BLKSIZE, 0);
   read(fd, buf, BLKSIZE);
}

int search(INODE* ip, char *name){ 
	char dbuf[BLKSIZE], temp[256];
	DIR *dp;
	char *cp;
	int dev = fd;
	int j;
	for (j=0; j < 12; j++){  // assume at most 12 direct blocks
		printf("ip->i_block[%d] =  %d\n",j, ip->i_block[j]);
		if (ip->i_block[j] == 0){
			return 0;
		}
		get_block(dev, ip->i_block[j], dbuf);
		dp = (DIR *)dbuf;
		cp = dbuf;
		printf("i_number rec_len  name_len  name\n");
		while (cp < dbuf + BLKSIZE){
		   strncpy(temp, dp->name, dp->name_len);
		   temp[dp->name_len] = 0;
		   printf("%4d     %4d      %4d      %s\n", \
			   dp->inode, dp->rec_len, dp->name_len, temp);
		   if(!strcmp(name,temp)){
		       return dp->inode;	
			}
			//printf("dp->rec_len = %d\n", (int)dp->rec_len);
		   cp += dp->rec_len;
		   dp = (DIR *)cp;
		}
  	}
} 


int main(int argc, char* argv[]){
	if (argc < 3){
		printf("Usage %s diskimage pathname\n", argv[0]);
		return 0;
	}
	printf("Searching disk %s for path %s\n", argv[1], argv[2]);
	char *disk = argv[1];
	n = tokenize(argv[2]);
	for (i=0; i<n; i++) 
		printf("path[%d] -> %s\n", i,path[i]); 
    fd = open(disk, O_RDONLY);
    if (fd < 0){
      printf("open %s failed\n", disk);
      exit(1);
    }
    int ino, blk, offset;
    char ibuf[BLKSIZE];
	get_block(fd, 1, ibuf);
	get_block(fd, 2, ibuf);
	gp = (GD *)ibuf;
	iblock = gp->bg_inode_table;
	printf("inode_block = %d\n", iblock);
	get_block(fd,iblock,ibuf);
	ip = (INODE *)ibuf + 1;
	printf("i_block[0] = %d\n", ip->i_block[0]);
                                                                          
	printf("n = %d\n", n);
    for (i=0; i < n; i++){
		printf("path[%d] = %s\n", i, path[i]);
		printf("Searching for %s\n",path[i]);
        ino = search(ip, path[i]);
        if (ino==0){
			printf("Returned 0 on i -> %d\n", i);
           printf("Couldn't find %s\n", path[i]); exit(1);
        }
       	printf("Found %s : ino = %d\n", path[i], ino);                            
        // Mailman's algorithm: Convert (dev, ino) to inode pointer
        blk    = (ino - 1) / 8 + iblock;  // disk block contain this INODE 
        offset = (ino - 1) % 8;         // offset of INODE in this block
        get_block(fd, blk, ibuf);
        ip = (INODE *)ibuf + offset;    // ip -> new INODE
    }
	i = 0;
	while(ip->i_block[i] != 0){
		printf("i_block[%d] -> %d\n",i, ip->i_block[i]);
		i++;
	}	
	int *intp;	
	if ((int)ip->i_block[12] != 0){
		printf("====INDIRECT BLOCKS====\n");
		get_block(fd,ip->i_block[12],ibuf);
		intp = ibuf;
		while (*(int*)intp > 0){
			printf("%d ", *intp);		
			intp++;
		}
		printf("\n====END OF INDIRECT BLOCKS====\n");	
	}
	if((int)ip->i_block[13] != 0){ 
			printf("====DOUBLE INDIRECT BLOCKS====\n");
			int blocks = ip->i_blocks;
			char *dindblk = (char *)ip->i_block[13]+1;

			char *sind = (char *)ip->i_block[13];
			
			char *dind = sind;

			i = 0;
			while(i<256 && i<blocks/2){
				int j = 0;
				while(j<256&& j<blocks/2){
					printf("%d ", dind);
					dind++;
					j++;
					if (j%10 ==0) printf("\n");
				}
				sind++;
				i++;
			}
			printf("\n====END DOUBLE INDIRECT BLOCKS====\n");
	}
/*
	int blocks =  ip->i_blocks;
	//printf("Blocks %d\n", blocks);	
    char buf[BLKSIZE];
	if((int)ip->i_block[13] != 0){
		printf("====DOUBLE INDIRECT BLOCKS====\n");
		get_block(fd,ip->i_block[13],ibuf);
		int **dintp = (int**)ibuf;
		i = 0;
		while (i < 256){
			if(dintp[i] == 0){break;}
			get_block(fd, dintp[i], buf);
			int *insidedintp = buf;
			int j = 0;
			while(j<256){
				if(insidedintp[j] == 0){break;}
				printf("%d ", insidedintp[j]);
				j++;
				if(j%10 == 0){ printf("\n"); }
			}
			printf("\n");
			i++;
		}	
	}
*/


}
