//Christopher Young
//CptS360 Program1
#include <stdio.h>
#include <stdlib.h>
int myprintf(char *fmt, ...);
int *FP;

int main(int argc,char *argv[],char *env[])
{
	
	myprintf("Main starting...\n");
	int a=0,b=0,c=0;
	myprintf("&argc=%x argv=%x env=%x\n", &argc,argv,env);
	printf("&a=%8x &b=%8x &c=%8x\n",&a,&b,&c);
	printf("%d\n",argc);
	for(int i = 0; i < argc; i++){
		printf("%s",argv[i]);	
	}
	printf("\n");
	a=1;b=2;c=3;
	A(a,b);
	printu(10);
	printf("\n");
	
	printd(-10);
	printf("\n");
	printx(10);
	printf("\n");
	printo(10);
	printf("\n");
	
	myprintf("cha=%c string=%s dec=%d hex=%x oct=%o neg=%d\n",
			'A',"this is a test",100,100,100,-100);
	printf("Out of main...\n");
}	

int A(int x,int y){
	int d,e,f;
	printf("A starting...\n");
	printf("&d=%8x &e=%8x &f=%8x\n",&d,&e,&f);
	d=4;e=5;f=6;
	B(d,e);
	printf("Out of A...\n");
}

int B(int x, int y){
	int g,h,i;
	printf("B starting...\n");
	printf("&g=%8x &h=%8x &i=%8x\n",&g,&h,&i);
	g=7;h=8;i=9;
	C(g,h);
	printf("Out of B...\n");

}

int C(int x,int y){
	int *p;
	int u,v,w,i;
	printf("C starting...\n");
	printf("&u=%8x &v=%8x &w=%8x,&i=%8x,&p=%8x\n",&u,&v,&w,&i,p);
	u=10;v=11;w=12;i=13;
	FP = (int *)getebp();
	while(*FP != 0){
		printf("FP = %8x     *FP = %8x\n",FP,*FP);
		FP = (int*)*FP;
	}
	printf("FP = %8x     *FP = %8x\n",FP,*FP);
	printf("Enter a character to move on: ");	
	char pause;
	scanf("%s",pause);
	p = (int *)&p;
	while (p != FP){
		printf("p=%8x     *p=%8x\n",p,*p);
		p++;
	}
}


typedef unsigned int u32;
char *ctable = "0123456789ABCDEF";

int rpu(u32 x, int base){
	char c;
	if (x){
		c = ctable[x % base];
		rpu(x/base, base);
		putchar(c);
	}
}

int printu(u32 x)
{
	//Print unsigned int
	(x==0)? putchar('0') : rpu(x,10);
	putchar(' ');
}

int printd(int x)
{
	//Print signed decimal value
	if (x<0){putchar('-');rpu(x*-1,10);}
	else{(x==0)? putchar('0') : rpu(x,10);}

}

int printx(u32 x){
	//Print hex value
	printf("0x");
	(x==0)? putchar('0') : rpu(x,16);
}

int printo(u32 x){
	//Print octal value
	printf("0");
	(x==0)? putchar('0') : rpu(x,8);
}


int (*fptr[])(int *)={(int (*) () )putchar,printf,printu,printd,printo,printx};
int findCmd(char *command){
	char *cmd[] = {"c","s","u","d","o","x",NULL};
	int i = 0;
	while(cmd[i]){
		if(*command==*cmd[i]){
			return i;
		}
		i++;
	}
	return -1;
}
int myprintf(char *fmt, ...){
	char *cp =fmt;
	int *ip =(int *)&fmt;
	int index = 0, r = 0;	
	ip++;

	while(*cp!='\0'){
		if (*cp == '%'){
			cp++;
			index = findCmd(cp);
			r = fptr[index](*ip);		
			ip++;
			cp++;
			
		}			
		printf("%c",*cp);
		cp++;
	}
}
