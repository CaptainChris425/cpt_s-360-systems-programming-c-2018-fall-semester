#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>

#define BLKSIZE 4096
struct stat *buf1;
struct stat *buf2;

int myrcp(char *, char *);
int cpd2d(char *, char *);
int cpf2f(char *, char *);
int cpf2d(char *, char *);
int dircontainsdir(char *, char*);

int main(int argc, char *argv[]){
	if(argc < 3){
		printf("Useage %s {file1} {file2}\n", argv[0]);
		exit(0);
	}
	return myrcp(argv[1], argv[2]);
}

int myrcp(char* f1, char* f2){
	buf1 = malloc(sizeof(struct stat));
	int r =	lstat(f1, buf1);
	if(r< 0) {printf("Cannot stat %s\n", f1); exit(0); }	
	buf2 = malloc(sizeof(struct stat));
	r =	lstat(f2, buf2);
//Reject f2 is it exists and is not a REG or LNK file
	if(S_ISLNK(buf1->st_mode)){
	//IF file1 is a link
		if(r<0){
			char link[256];
			ssize_t len = readlink(f1,link,256);
			link[len] = '\0';
			symlink(link,f2);
		}else{
			printf("%s already exists, rejecting\n", f2);	
			return(0);
		}
	}else if((buf1->st_mode & 0xF000) == 0x8000){
	//IF FILE1 IS A REGISTER
		if(r<0 || (buf2->st_mode & 0xF000) == 0x8000){
		//IF f2 doesnt exist or is a register
			return cpf2f(f1, f2);
		}else if((buf2->st_mode & 0xF000) == 0x4000)
		//IF f2 exists and is a dir
			return cpf2d(f1,f2);
	}else if((buf1->st_mode & 0xF000) == 0x4000){
	//IF file1 is a dir
		
		if(r < 0){
			if(mkdir(f2, 0766)==0)
				printf("%s doesnt exist, making dir\n",f2);
		}else if((buf1->st_mode & 0xF000) != 0x4000){
			printf("Must reject %s, is not dir\n", f2);	
			return(0);
		}else if(buf1->st_ino == buf2->st_ino){
			printf("%s and %s are the same directory\n",f1,f2);
			exit(0);
		}else if(dircontainsdir(f1,f2)){
			exit(0);
		}
		else {
			strcat(f2,"/");
			strcat(f2,f1);
			if(mkdir(f2, 0766)==0) printf ("Made dir %s\n",f2);
		}

			
		return cpd2d(f1,f2);
	}
}

int cpf2f(char *f1, char *f2){
	buf1 = malloc(sizeof(struct stat));
	buf2 = malloc(sizeof(struct stat));
	int r =	lstat(f1, buf1);
	r =	lstat(f2, buf2);
//if the files are the same or linked as the same
	if(buf1->st_dev == buf2->st_dev || buf1->st_ino == buf2->st_ino	){
		printf("%s and %s are the same file\n", f1,f2);
		return(1);
	}
	if((buf1->st_mode & 0xF000) == 0xA000){
	//IF file1 is a link
	}
	int fd, gd, n, total=0;
	char buf[BLKSIZE];
	if((fd = open(f1,O_RDONLY))< 0)
		return(2);
	if((gd = open(f2,O_WRONLY|O_CREAT|O_TRUNC,buf1->st_mode))<0)
		return(3);
	while(n = read(fd,buf,BLKSIZE)){
		write(gd,buf,n);
		total += n;
	}
	printf("Total bytes copied = %d\n", total);
	close(fd); close(gd);
}

int cpf2d(char *f1, char *f2){
	buf1 = malloc(sizeof(struct stat));
	buf2 = malloc(sizeof(struct stat));
	int r =	lstat(f1, buf1);
	r =	lstat(f2, buf2);
	struct dirent *ep;
	DIR *dp = opendir(f2);
	char *bn = basename(f1);
	char nf1[256], nf2[256];
	while(ep = readdir(dp)){
		if(!strcmp(ep->d_name,bn)){
			if(S_ISDIR(buf1->st_mode)){
				strcpy(nf2, f2);
				cpf2d(f1, strcat(strcat(nf2,"/"),bn));
			}
			else{	
				printf("%s was found in %s\n", f1, f2);
				return(1);
			}
		}
	}	
	strcpy(nf2, f2);
	cpf2f(f1, strcat(strcat(nf2,"/"),bn));
	return 0;
}

int dircontainsdir(char *f1, char*f2){
	char nf2[256], nf1[256];
	char dn[256];
	strcpy(nf2,f2);
	strcpy(dn, strtok(nf2,"/"));
	if(!strcmp(f1,dn)){
		printf("Cannot copy into subdir\n");
		return 1;
	}
	return 0;
	
/*
	struct dirent *ep;
	DIR *dp;
	char *bn = basename(f1);
	char nf2[256], nf1[256];
	char dn[256];
	strcpy(nf2,f1);
	strcpy(dn, strtok(nf2,"/"));
	printf("dn = %s\n",dn);
	strcpy(nf1, f1);
	strcpy(nf2, f2);
	struct stat *bufr;
	struct stat *bufc;
	char c;
	bufr = malloc(sizeof(struct stat));
	bufc = malloc(sizeof(struct stat));

	int r =  lstat(nf2, bufc);
	r = lstat("~",bufr);
	while(bufc->st_ino != 2){
		dp = opendir(nf2);
		while(ep = readdir(dp)){
			if (!strcmp(dn, ep->d_name)){
				printf("Cannot copy into subdir\n");
				exit(0);	
			}

		}	
		strcpy(nf2,strcat(strcat(nf2,"/"),".."));
		r =  lstat(nf2, bufc);
	}
*/
}	

int cpd2d(char *f1, char *f2){
	//dircontainsdir(f1,f2);
	buf1 = malloc(sizeof(struct stat));
	int r;
	struct dirent *ep;
	DIR *dp = opendir(f1);
	char *bn = basename(f2);
	char nf1[256], nf2[256];
	while(ep = readdir(dp)){
		strcpy(nf1,f1);
		strcat(strcat(nf1,"/"),ep->d_name);
	
		r =	lstat(nf1, buf1);		
		if (!strcmp(ep->d_name,".") ||!strcmp(ep->d_name,"..")){
		}else
		if(S_ISDIR(buf1->st_mode)){
			strcpy(nf2,f2);
			mkdir(strcat(strcat(nf2,"/"),ep->d_name),0766);
			cpd2d(nf1,nf2);	
			//printf("%s is a dir\n",strcat(strcat(f2,"/"),bn));
			//cpd2d(f1, strcat(strcat(f2,"/"),bn));
			//return 0;	
		}
		else if (S_ISREG(buf1->st_mode)){
			cpf2d(nf1,f2);
		}else if(S_ISLNK(buf1->st_mode)){
			cpf2d(nf1,f2);
		}

	}
	return 0;
}
