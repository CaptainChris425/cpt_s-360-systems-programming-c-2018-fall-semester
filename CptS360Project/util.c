#ifndef UTILS_H
#define UTILS_H

//file control operations
#include <fcntl.h>
//ext2 file opertaions
#include <ext2fs/ext2_fs.h>
//unix standard libary
#include <unistd.h>
//Unix datatypes with shorter names
#include "type.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>
#include <time.h>

/**** globals defined in main.c file ****/
/*
extern 
extern 
extern 
extern 
extern 
extern 
extern 
extern 
extern 
*/

MINODE minode[NMINODE];
MINODE *root;
PROC   proc[NPROC], *running;
char gpath[128];
char *name[64];
int n;
int fd, dev;
int nblocks, ninodes, bmap, imap, inode_start;
char line[256], cmd[32], pathname[256];




int tokenize(char *pathname)
{
  char *s;
  strcpy(gpath,pathname);
  n = 0;
  s = strtok(gpath, "/");
  while(s){
	name[n++] = s;
	s = strtok(0,"/");
  }
} 
 // tokenize pathname into n components: name[0] to name[n-1];


MINODE *mialloc()
{
	int i;
	for (i=0;i<NMINODE; i++){
		MINODE* mp = &minode[i];
		if (mp->refCount == 0){
			mp->refCount = 1;
			return mp;
		}
	}
	printf("No more availiable minodes...\n");
	return 0;
}

int midalloc(MINODE* mip)
{
	mip->refCount = 0;
}

int get_block(int dev, int blk, char buf[ ])
//returns the block (with block numebr blk) inside of buf
{
	lseek(dev, (long)blk*BLKSIZE, 0);
	int n = read(dev, buf, BLKSIZE);
	if (n<0) printf("{get_block} [%d %d] error\n", dev,blk);
}

int put_block(int dev, int blk, char buf[])
{
	lseek(dev, (long)blk*BLKSIZE, 0);
	int n = write(dev, buf, BLKSIZE);
	printf("n -> %d blksize -> %d\n", n, BLKSIZE);
	if (n != BLKSIZE)
		printf("{put_block} [%d %d] error\n", dev,blk);
}   

MINODE *iget(int dev, int ino)
{
  MINODE *mip;
  int i, block, offset;
  char buf[BLKSIZE];
  // return minode pointer to loaded INODE
/*
  (1). Search minode[ ] for an existing entry (refCount > 0) with 
       the needed (dev, ino):
       if found: inc its refCount by 1;
                 return pointer to this minode;
*/
  for (i = 0; i<NMINODE; i++){
	mip = &minode[i];
	if (mip->refCount > 0 && mip->dev == dev && mip->ino == ino){
		mip->refCount++;
		return mip;	
		}
	}
/*

  (2). // needed entry not in memory:
       find a FREE minode (refCount = 0); Let mip-> to this minode;
       set its refCount = 1;
       set its dev, ino
*/
/*
	}
  }
  (3). load INODE of (dev, ino) into mip->INODE:
       
       // get INODE of ino a char buf[BLKSIZE]    
       blk    = (ino-1) / 8 + inode_start;
       offset = (ino-1) % 8;

       printf("iget: ino=%d blk=%d offset=%d\n", ino, blk, offset);

       get_block(dev, blk, buf);
       ip = (INODE *)buf + offset;
       mip->INODE = *ip;  // copy INODE to mp->INODE

       return mip;
*/
  for (i = 0; i<NMINODE; i++){
	mip = &minode[i];
	if (mip->refCount == 0){
		mip->refCount = 1;	
		mip->dev = dev; 
		mip->ino = ino;
		block = (ino-1)/8 + inode_start;
		offset = (ino-1)%8;
		get_block(dev, block, buf);
		ip = (INODE *)buf + offset;
		mip->INODE = *ip;
		mip->mounted = 0;
		mip->dirty = 0;
		mip->mounted = 0;
 		mip->mptr = 0;
		return mip; 
		
	}
  }
  printf("No minodes avaliable to get\n");
  return 0;
}


int iput(MINODE *mip) // dispose a used minode by mip
{
	printf("in iput...\n");
	int i, block, offset;
	INODE* ip;
    char buf[BLKSIZE];
	if(mip == 0){
		printf("Nothing to put back\n");	
		return 0;
	}
	mip->refCount--;
	if (mip->refCount > 0){
		printf("Still has a user\n");		
		return 0;
	}
	if (!mip->dirty){
		printf("Not dirty\n");
		return 0;
	} 
	// Write YOUR CODE to write mip->INODE back to disk
	block = (mip->ino - 1) /8 + inode_start;
	offset = (mip->ino -1)%8;
	
	get_block(mip->dev,block,buf);
	ip = (INODE*)buf + offset;
	*ip = mip->INODE;
	printf("iput put_block\n");
	put_block(mip->dev, block, buf);
	midalloc(mip);
} 


// serach a DIRectory INODE for entry with a given name
int search(MINODE *mip, char *name)
{
	int i;
	char * cp, temp[256], sbuf[BLKSIZE];
	for (i=0;i<12;i++){
		if(mip->INODE.i_block[i] == 0){
			return 0;
		}
		get_block(mip->dev, mip->INODE.i_block[i], sbuf);
		dp = (DIR *)sbuf;
		cp = sbuf;
		while (cp < sbuf + BLKSIZE){
			strncpy(temp, dp->name, dp->name_len);
			temp[dp->name_len] = 0;
			
			if (strcmp(name,temp) == 0){
				return dp->inode;
			}
			cp += dp->rec_len;
			dp = (DIR *)cp;
		}
	}
	return 0;
}
   // return ino if found; return 0 if NOT



// retrun inode number of pathname

int getino(char *pathname)
{ 
	MINODE *mip;
	int i,ino;
	if(strcmp(pathname,"/") == 0) return 2; //If the pathname is / then return the root
	if(pathname[0] == '/') mip = root; //If the pathname starts with / then the directory is in the
	else mip = running->cwd; //If it does not have a / at the start then go to cwd
	mip->refCount++; //Increase its ref count
	tokenize(pathname); //Tokenize the pathname

	for (i=0;i<n;i++){
		if(!S_ISDIR(mip->INODE.i_mode)){
			printf("%s is not a directory\n", name[i]);
		    iput(mip);
			return 0;
		}
		ino = search(mip, name[i]);
		if(!ino){
			printf("%s componenet name was not found\n",name[i]);
			iput(mip);
			return 0;
		}
		iput(mip);
		mip = iget(dev,ino);
	}
	iput(mip);
	return ino;
   // SAME as LAB6 program: just return the pathname's ino;
}



// THESE two functions are for pwd(running->cwd), which prints the absolute
// pathname of CWD. 

int findmyname(MINODE *mip, u32 myino, char *myname) 
{
   // parent -> at a DIR minode, find myname by myino
   // get name string of myino: SAME as search except by myino;
   // copy entry name (string) into myname[ ];
	int i;
	char * cp, temp[256], sbuf[BLKSIZE];
	for (i=0;i<12;i++){
		if(mip->INODE.i_block[i] == 0){
			return 0;
		}
		get_block(mip->dev, mip->INODE.i_block[i], sbuf);
		dp = (DIR *)sbuf;
		cp = sbuf;
		while (cp < sbuf + BLKSIZE){
	//		printf("inode (%d) == myino (%d)\n",dp->inode,myino);
			if(dp->inode == myino){
				strncpy(myname, dp->name, dp->name_len);
				return 0;
			}
			cp += dp->rec_len;
			dp = (DIR *)cp;
		}
	}
	return 0;
}


int findino(MINODE *mip, u32 *myino) 
{
  // fill myino with ino of . 
  // retrun ino of ..
}

//Super pointer to check for ext2 magic number


SUPER* super(int fd, char buf[], SUPER *sp)
//Returns a pointer of the super block of an ext2 filesystem
{
	get_block(fd, 1, buf);
	sp = (SUPER *)buf;
	if (sp->s_magic != SUPER_MAGIC)
		return 0;
	return sp;
}	

GD* groupdesc(int fd, char buf[], GD *gp)
//Returns a pointer of the group descriptor block of an ext2 filesystem
{
	get_block(fd,2,buf);
	gp = (GD *)buf;
	return gp;
}	

int tst_bit(char *buf, int bit)
{
  int i, j;
  i = bit/8; j=bit%8;
  if (buf[i] & (1 << j))
     return 1;
  return 0;
}

int set_bit(char *buf, int bit)
{
  int i, j;
  i = bit/8; j=bit%8;
  buf[i] |= (1 << j);
}

int clr_bit(char *buf, int bit)
{
  int i, j;
  i = bit/8; j=bit%8;
  buf[i] &= ~(1 << j);
}


	
int decFreeBlocks(int dev)
{
  char buf[BLKSIZE];

  // dec free inodes count in SUPER and GD
  get_block(dev, 1, buf);
  sp = (SUPER *)buf;
  sp->s_free_blocks_count--;
  put_block(dev, 1, buf);

  get_block(dev, 2, buf);
  gp = (GD *)buf;
  gp->bg_free_blocks_count--;
  put_block(dev, 2, buf);
}

int balloc(int dev)
{
  int  i;
  char buf[BLKSIZE];

  // read inode_bitmap block
  get_block(dev, bmap, buf);

  for (i=0; i < nblocks; i++){
    if (tst_bit(buf, i)==0){
       set_bit(buf,i);
       decFreeBlocks(dev);

       put_block(dev, bmap, buf);

       return i+1;
    }
  }
  printf("balloc(): no more free blocks\n");
  return 0;
}

int decFreeInodes(int dev)
{
  char buf[BLKSIZE];

  // dec free inodes count in SUPER and GD
  //get_block(dev, 1, buf);
  //sp = (SUPER *)buf;
  sp = super(fd,buf,sp);
  sp->s_free_inodes_count--;
  put_block(dev, 1, buf);

  get_block(dev, 2, buf);
  gp = (GD *)buf;
  gp->bg_free_inodes_count--;
  put_block(dev, 2, buf);
}

int ialloc(int dev)
{
  int  i;
  char buf[BLKSIZE];

  // read inode_bitmap block
  get_block(dev, imap, buf);

  for (i=0; i < ninodes; i++){
    if (tst_bit(buf, i)==0){
       set_bit(buf,i);
       decFreeInodes(dev);
	   printf("put_block(dev,imap,buf)\n");
       put_block(dev, imap, buf);

       return i+1;
    }
  }
  printf("ialloc(): no more free inodes\n");
  return 0;
}





#endif 
