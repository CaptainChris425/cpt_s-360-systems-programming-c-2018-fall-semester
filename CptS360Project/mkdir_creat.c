#include "util.c"
//#include "util.o"

/*
Function:
Parameters:
Action:
*/


/*
Function: enter_name
Parameters: Parent inode pointer, ino for directory, name of dir
Action: Enters the new directory as a directory of the parent
*/
int enter_name(MINODE* pip, int myino, char *myname)
{
	int i;
	int ideal_length,need_length, remain;
	char buf[BLKSIZE];
	char *cp;
	INODE *ip;
	ip = &pip->INODE; //get parents INODE
	for (i = 0; i<12; i++) //for each direct blocks in the parent dir 
	{
		if(ip->i_block[i] == 0)
			break;	
		get_block(pip->dev, ip->i_block[i], buf); //get datablock into buf
		dp = (DIR *)buf; //dp points to the directory stored in buf
		cp = buf; //cp points to the buf
		int blk = ip->i_block[i];
		printf("step to last entry in data block -> %d\n",blk);
		while(cp+dp->rec_len < buf + BLKSIZE) //step to last entry in data block
		{
		/****** Technique for printing, compare, etc.******
		*/
          char c = dp->name[dp->name_len];
          dp->name[dp->name_len] = 0;
          printf("%s ", dp->name);
          dp->name[dp->name_len] = c;
          /***************************************************/
          cp += dp->rec_len;
          dp = (DIR *)cp;			

		}
		printf("\n");
		//Now dp points at the last entry in block	
		ideal_length = 4*( (8 +	dp->name_len + 3) /4 ); //ideal length of a record is factor of its name length
		need_length = 4*( (8 +	strlen(myname) + 3) /4 ); //length needed for new dir
		remain = dp->rec_len - ideal_length; //last entry has length of remaining space
			//calculate how much remains by taking away the idea length of the record
			//from its total length
		if(remain>=need_length) //if there is enough space for the new dir
		{
			printf("remain (%d) >=needlength (%d) \n", remain,need_length);
			dp->rec_len = ideal_length;
			//bzero(buf, BLKSIZE);
			//dp = (DIR *)buf;
			cp += dp->rec_len;
			dp = (DIR*)cp;
			dp->inode = myino;
			dp->rec_len = remain;
			dp->name_len = strlen(myname);
			strcpy(dp->name,myname);
			put_block(dev,blk,buf);	
		}	

	}	
}




/*
Function: mymkdir
Parameters: parent ino and new dir name
Action: This is called after it is confirmed that a dir can be made here with that name
		Makes the new dir in the parent directory
*/
int mymkdir(MINODE *pip, char *name)
{
	int i;
	int ino, bno; //inode number and block number for new directory
	MINODE *mip; //Pointer to the new memory inode for the new dir
	INODE *ip; //To set the INODE information in mip
	char buf[BLKSIZE]; //buffer to hold data blocks for . and ..
printf("dev -> %d fd -> %d\n",dev,fd);
printf("Getting ino\n");
	ino = ialloc(dev); //get a free ino
printf("ino -> %d\n",ino);
	bno = balloc(dev); //get a free block number
printf("bno -> %d\n",bno);
	mip = iget(dev,ino); //points at the new directories MINODE
printf("Got mip\n");
	ip = &mip->INODE; //ip now is the new dir's inode
printf("Got ip\n");
	
	//SETTING THE INODE TO BE A DIRECTORY
	ip->i_mode = 0x41ED;		// OR 040755: DIR type and permissions
	ip->i_uid  = running->uid;	// Owner uid 
	ip->i_gid  = running->gid;	// Group Id
	ip->i_size = BLKSIZE;		// Size in bytes 
	ip->i_links_count = 2;	        // Links count=2 because of . and ..
	ip->i_atime = ip->i_ctime = ip->i_mtime = time(0L);  // set to current time
	ip->i_blocks = 2;                	// LINUX: Blocks count in 512-byte chunks 
	ip->i_block[0] = bno;             // new DIR has one data block   
	for (i=1;i<15;i++)
		ip->i_block[i] = 0;
	mip->dirty = 1;               // mark minode dirty
	printf("iput(mip)\n");
	iput(mip);                    // write INODE to disk
	enter_name(pip,ino,name);	
	//MAKING NEW DATA BLOCK FOR DIR CONTAINING . AND .. ENTRIES
	//mip->INODE[0				

		
}


/*
Function: make_dir
Parameters: A pathname -> "/a/b/c" or "a/b/c"
Action: Makes a directory if possible at the pathname
Simulates linux mkdir
*/
int make_dir(char pathname[])
{
	MINODE *pip; //MINODE pointer to parent dir
	INODE *ip; //to update parent time
	int pino; //parent inode number
	char *parent, *child; //Parent for the containing dir, child for the new dir
	char tempDirname[256]; //temporary pathname for when pathname gets destroyed
	char tempChildname[256]; //temporary pathname for when pathname gets destroyed
	strcpy(tempDirname, pathname); //copy pathname into temp path
	printf("%s\n",tempDirname); //
	parent = dirname(tempDirname); //the pathname up until the last slash
	strcpy(tempChildname, pathname); //copy pathname into temp path
	printf("%s\n",tempChildname); //
	child = basename(tempChildname); //everything after the last slash
	printf("parent -> %s\n",parent);
	printf("child -> %s\n",child);
	pino = getino(parent); //get ino of parent directory
	printf("Parent ino -> %d\n",pino);
	if (!pino) //pino not found
	{
		printf("Path to parent was not found\n");
		return 0;
	}
	pip = iget(dev,pino); //let pip point at the MINODE of parent	
	if (!S_ISDIR(pip->INODE.i_mode)) //if the parent MINODE is not a directory (i.e. is a file)
	{
		printf("Parent path is not a directory\n");
		iput(pip);
		return 0;
	}
	if (search(pip,child)) //if search returns 0 (child not found in parent directory)
	{
		printf("Child already exists in parent directory\n");
		iput(pip);
		return 0;
	}
	printf("Ready to makedir...\n");
	mymkdir(pip, child); //all checks have passed, ready to make dir		
	ip = &pip->INODE;
	ip->i_atime = time(0L);  // set to current time
	pip->dirty = 1; //mark parent minode as dirty 
	printf("iput(pip)\n");
	iput(pip);


}

int mycreat(MINODE *pip, char *name)
{
	int i;
	int ino, bno; //inode number and block number for new directory
	MINODE *mip; //Pointer to the new memory inode for the new dir
	INODE *ip; //To set the INODE information in mip
	char buf[BLKSIZE]; //buffer to hold data blocks for . and ..
printf("dev -> %d fd -> %d\n",dev,fd);
printf("Getting ino\n");
	ino = ialloc(dev); //get a free ino
printf("ino -> %d\n",ino);
	mip = iget(dev,ino); //points at the new file MINODE
printf("Got mip\n");
	ip = &mip->INODE; //ip now is the new dir's inode
printf("Got ip\n");
	
	//SETTING THE INODE TO BE A DIRECTORY
	ip->i_mode = 0x0644;		// OR 040755: DIR type and permissions
	ip->i_uid  = running->uid;	// Owner uid 
	ip->i_gid  = running->gid;	// Group Id
	ip->i_size = BLKSIZE;		// Size in bytes 
	ip->i_links_count = 1;	        // Links count=2 because of . and ..
	ip->i_atime = ip->i_ctime = ip->i_mtime = time(0L);  // set to current time
	ip->i_blocks = 2;                	// LINUX: Blocks count in 512-byte chunks 
	ip->i_block[0] = bno;             // new DIR has one data block   
	for (i=1;i<15;i++)
		ip->i_block[i] = 0;
	mip->dirty = 1;               // mark minode dirty
	printf("iput(mip)\n");
	iput(mip);                    // write INODE to disk
	enter_name(pip,ino,name);	
	//MAKING NEW DATA BLOCK FOR DIR CONTAINING . AND .. ENTRIES
	//mip->INODE[0				

		
}

int create_file(char pathname[])
{
	MINODE *pip; //MINODE pointer to parent dir
	INODE *ip; //to update parent time
	int pino; //parent inode number
	char *parent, *child; //Parent for the containing dir, child for the new dir
	char tempDirname[256]; //temporary pathname for when pathname gets destroyed
	char tempChildname[256]; //temporary pathname for when pathname gets destroyed
	strcpy(tempDirname, pathname); //copy pathname into temp path
	printf("%s\n",tempDirname); //
	parent = dirname(tempDirname); //the pathname up until the last slash
	strcpy(tempChildname, pathname); //copy pathname into temp path
	printf("%s\n",tempChildname); //
	child = basename(tempChildname); //everything after the last slash
	printf("parent -> %s\n",parent);
	printf("child -> %s\n",child);
	pino = getino(parent); //get ino of parent directory
	printf("Parent ino -> %d\n",pino);
	if (!pino) //pino not found
	{
		printf("Path to parent was not found\n");
		return 0;
	}
	pip = iget(dev,pino); //let pip point at the MINODE of parent	
	if (!S_ISDIR(pip->INODE.i_mode)) //if the parent MINODE is not a directory (i.e. is a file)
	{
		printf("Parent path is not a directory\n");
		iput(pip);
		return 0;
	}
	if (search(pip,child)) //if search returns 0 (child not found in parent directory)
	{
		printf("Child already exists in parent directory\n");
		iput(pip);
		return 0;
	}
	printf("Ready to creat...\n");
	mycreat(pip, child); //all checks have passed, ready to make dir		
	ip = &pip->INODE;
	ip->i_links_count++; //increment parent links count since it has a new dir now
	ip->i_atime = time(0L);  // set to current time
	pip->dirty = 1; //mark parent minode as dirty 
	printf("iput(pip)\n");
	iput(pip);


}
/*

int main()
{
	printf("hello world\n");
	make_dir("/aaa/bbb/ccc");
	
}

*/
