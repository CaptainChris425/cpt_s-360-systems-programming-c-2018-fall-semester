#ifndef CD_LS_PWD_c
#define CD_LS_PWD_c
int ls(char *dirname)
{
	if (strcmp(dirname, "") == 0){ //if no name is specified
		char buf[BLKSIZE];
		INODE *ip;
		MINODE *mip;
		mip = running->cwd;
		ip = &mip->INODE;	
		get_block(mip->dev, ip->i_block[0], buf);	
		dp = (DIR *)buf;
		strcpy(dirname, dp->name);
	}
    int i;
    char * cp, temp[256], sbuf[BLKSIZE];
	int ino = getino(dirname);
	if (!ino){
		printf("Not a dir entry\n");
		return 0;
	}
	char file_name[EXT2_NAME_LEN+1];
	char buf[BLKSIZE];
	MINODE *mip = iget(dev,ino);
    for (i=0;i<12;i++){
//		printf("ls i-> %d\n",i);
        if(mip->INODE.i_block[i] == 0){
			printf("Ls iput\n");
			iput(mip);
            return 0;
        }
        get_block(mip->dev, mip->INODE.i_block[i], sbuf);
        dp = (DIR *)sbuf;
        cp = sbuf;
        while (cp < sbuf + BLKSIZE){
			//printf("cp = %s || sbuf+blksize = %s\n", cp, sbuf+BLKSIZE);
			//printf("dp->reclen = %d", dp->rec_len);
            strncpy(temp, dp->name, dp->name_len);
            temp[dp->name_len] = 0;
			printf("%s\n",temp);
            cp += dp->rec_len;
            dp = (DIR *)cp;
        }
    }
	printf("Ls iput\n");
	iput(mip);
    return 0;
}

int rpwd(MINODE *wd)
{
	running->cwd = wd;
	if (wd == root) return 0;
	int myino, parentino;
	char myname[256], nbuf[BLKSIZE];
	MINODE *pip;
	myino = getino(".");
	parentino = getino("..");
	pip = iget(dev, parentino);
	get_block(pip->dev, pip->INODE.i_block[0],nbuf);
	dp = (DIR *)nbuf;	
	findmyname(pip,myino,myname);
	//printf("before recursing myname = %s\n",myname);	
	myname[dp->name_len] = 0;
	rpwd(pip);
	printf("/%s",myname);	

}

int pwd(MINODE *wd)
{
	if (wd == root)
		printf("/\n");	
	else{
		rpwd(wd);
		printf("\n");
		running->cwd = wd;
	}
	return 0;
}

int cdir(char *pathname)
{
	int ino;
	MINODE *mip;
	if (strcmp(pathname, "") == 0){
		running->cwd = root;
		return 0;
	}
	ino = getino(pathname);
	mip = iget(dev, ino);
	if(!S_ISDIR(mip->INODE.i_mode)){
		printf("%s is not a directory\n",pathname);	
		return 0;
	}
	iput(running->cwd);
	running->cwd = mip;
	return 0;
}




#endif 
